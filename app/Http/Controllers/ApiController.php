<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Check phone exist or not in DB
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkPhone(Request $request)
    {
        if ($request->phone) {
            $phone = $request->phone;

            $sendOtpDetails = [];

            $userExist = User::where('phone', $phone)->first();

            if ($userExist) {
                $message = "exist";

            } else {
                $message        = "new";
                $sendOtpDetails = $this->sendOtp($phone);

            }

            return response()->json([
                'success' => true,
                'message' => $message,
                'phone' => $phone,
                'user_email' => ($userExist) ? $userExist->email : null,
                'user_firstname' => ($userExist) ? ucfirst($userExist->firstname) : null,
                'sendOtpDetails' => $sendOtpDetails

            ]);
        }

        return response()->json([
            'success' => false
        ]);
    }
}
